package com.codegreenllc.mongoservices.repos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.codegreenllc.mongoservices.model.Department;

@Repository
public interface DepartmentRepository extends MongoRepository<Department, String> {

	Department findByName(String name);

}