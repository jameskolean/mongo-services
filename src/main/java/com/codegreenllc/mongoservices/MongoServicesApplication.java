package com.codegreenllc.mongoservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoServicesApplication {

	public static void main(final String[] args) {
		SpringApplication.run(MongoServicesApplication.class, args);
	}
}
