package com.codegreenllc.mongoservices.controller;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codegreenllc.mongoservices.model.Department;
import com.codegreenllc.mongoservices.repos.DepartmentRepository;

@RestController
@RequestMapping("department")
public class DepartmentController {
	@Autowired
	private DepartmentRepository repository;

	private Sort buildSort(final String sort) {
		Sort by;
		if (Strings.isBlank(sort)) {
			by = Sort.by(Sort.Direction.ASC, "id");

		} else {
			by = Sort.by(Sort.Direction.ASC, sort);
		}
		return by;
	}

	@PostMapping()
	public Department create(@RequestBody final Department department) {
		repository.insert(department);
		return department;
	}

	@DeleteMapping("{id}")
	public String delete(@PathVariable("id") final String id) {
		repository.deleteById(id);
		return id;
	}

	@GetMapping()
	public Page<Department> getAll(@RequestParam final int page, @RequestParam final int size,
			@RequestParam(required = false) final String sort) {
		return repository.findAll(PageRequest.of(page, size, buildSort(sort)));
	}

	@GetMapping("{id}")
	public Department getById(@PathVariable("id") final String id) {
		return repository.findById(id).orElse(null);
	}

	@PatchMapping
	public Department update(@RequestBody final Department department) {
		if (!repository.existsById(department.id)) {
			return null;
		}
		return repository.save(department);
	}
}
