package com.codegreenllc.mongoservices.model;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Department {
	@Id
	public String id;
	int lazyValue;
	String name;
}
