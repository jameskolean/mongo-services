package com.codegreenllc.mongoservices.graphql.resolvers;

import org.springframework.stereotype.Component;

import com.codegreenllc.mongoservices.model.Department;
import com.coxautodev.graphql.tools.GraphQLResolver;

@Component
public class DepartmentResolver implements GraphQLResolver<Department> {

	public String getLazyValue(final Department department) {
		System.out.println("Fetching lazy value for department '" + department.getName() + "'");
		return "I'm lazy";
	}

}