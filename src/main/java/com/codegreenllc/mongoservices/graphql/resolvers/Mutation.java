package com.codegreenllc.mongoservices.graphql.resolvers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.codegreenllc.mongoservices.model.Department;
import com.codegreenllc.mongoservices.repos.DepartmentRepository;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;

@Component
public class Mutation implements GraphQLMutationResolver {
	@Autowired
	private DepartmentRepository departmentRepository;

	public Department createDepartment(final String name) {
		final Department department = new Department();
		department.setName(name);
		return departmentRepository.insert(department);
	}

	public String deleteDepartment(final String id) {
		departmentRepository.deleteById(id);
		return id;
	}

	public Department updateDepartment(final String id, final String name) {
		final Department department = new Department();
		department.setId(id);
		department.setName(name);
		return departmentRepository.save(department);
	}

}