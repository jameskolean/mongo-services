package com.codegreenllc.mongoservices.graphql.resolvers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.codegreenllc.mongoservices.model.Department;
import com.codegreenllc.mongoservices.repos.DepartmentRepository;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class Query implements GraphQLQueryResolver {
	@Autowired
	private DepartmentRepository departmentRepository;

	public List<Department> allDepartments(final int page, final int size, final DepartmentSortFieldEnum sort) {
		return departmentRepository.findAll(PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, sort.name())))
				.getContent();
	}

	public Department department(final String id) {
		return departmentRepository.findById(id).orElse(null);
	}

}