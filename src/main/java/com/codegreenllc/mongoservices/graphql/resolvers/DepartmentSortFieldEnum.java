package com.codegreenllc.mongoservices.graphql.resolvers;

public enum DepartmentSortFieldEnum {
	id, name;
}
